import React, { useState } from 'react';
import { debounce } from 'lodash'

import './Table.scss';

import { Button, InputText, TableRow } from '../../atoms';
import { Popup } from '../../molecules';
import { connect } from 'react-redux';
import { addProduct } from '../../../actions/products';

const Table = (props) => {
    const { data, addProduct } = props

    const [popupIsActive, setPopupIsActive] = useState(false);
    const [fieldsData, setFieldsData] = useState([]);
    const [sort, setSort] = useState({field: "default", type: 0});
    const [tableData, setTableData] = useState(data);

    const getDataFromField = debounce((name, value) => {
        let updatedFieldsData = fieldsData.concat()

        let currentFieldData = {
            name: name,
            value: value
        }

        let currentFieldIndex = updatedFieldsData.map(field => field.name).indexOf(name)

        currentFieldIndex === -1 ? (
            updatedFieldsData.push(currentFieldData)
        ) : (
            updatedFieldsData[currentFieldIndex].value = value
        )

        setFieldsData(updatedFieldsData)
    }, 200)

    const setDataFromForm = (product) => {
        let title = product.find(field => field.name === "title")
        let price = product.find(field => field.name === "price")
        let quantity = product.find(field => field.name === "quantity")

        // Если данные хранятся как массив массивов
        // let productqwe = []
        // productqwe.push(quantity ? Number(quantity.value) : 0)
        // productqwe.push(price ? price.value : "")
        // productqwe.push(title ? title.value : "")


        // Если данные хранятся как массив объектов
        let productqwe = {
            title: title ? title.value : "",
            price: price ? price.value : "",
            quantity: quantity ? Number(quantity.value) : ""
        }

        addProduct(productqwe)
    }

    const dataSort = (field) => {
        let newState = {
            field: field,
            type: 0
        }

        let updatedData;

        console.log("Start")
        console.log(tableData)

        if(sort.field === field) {
            if(sort.type === 0) { 
                if(field === "quantity") {
                    updatedData = tableData.concat()
                }

                newState.type = 1
            }

            if(sort.type === 1) { 
                if(field === "quantity") {
                    updatedData = tableData.concat().sort((a, b) => {
                        return a['quantity'] - b['quantity']
                    })
                }

                newState.type = 2 
            }
            
            if(sort.type === 2) { 
                if(field === "quantity") {
                    updatedData = tableData.concat().sort((a, b) => {
                        return a['quantity'] - b['quantity']
                    })
                }

                newState.field = "default"
                newState.type = 0 
            }
        }
        else { newState.type = 1 }

        // console.log(updatedData)
        console.log("Finish")

        // setTableData(updatedData)
        setSort(newState)
    }

    const rows = data.map((row, i) =>
        <TableRow id={row.id} title={row.title} price={row.price} quantity={row.quantity} key={i} />
    )

    const styles = {
        fields: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            flexWrap: "wrap",
            position: "relative",
            width: "100%"
        },
        tools: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            flexWrap: "wrap",
            position: "relative",
            width: "100%",
            padding: "8px 8px 0 8px"
        }
    }

    return(
        <div className="table">
            <div className="table__head">
                <div className="table-row__cell table-row__cell--title" onClick={() => {dataSort("title")}}>Наименование</div>
                <div className="table-row__cell table-row__cell--price" onClick={() => {dataSort("price")}}>Цена</div>
                <div className="table-row__cell table-row__cell--quantity" onClick={() => {dataSort("quantity")}}>Количество</div>
                <div className="table-row__cell table-row__cell--remove"></div>
            </div>
            <div className="table__body">
                {rows}
            </div>
            <Button text="Добавить товар" handleOnClick={() => setPopupIsActive(true)} />
                                               
            {!popupIsActive ? null : (
                <Popup width="400px" title="Добавление товара" closer={() => setPopupIsActive(false)}>
                    <div style={styles.fields}>
                        <InputText 
                            width="100%" 
                            name="title"
                            type="text" 
                            placeholder="Наименование" 
                            handleOnChange={getDataFromField}
                        />
                        <InputText 
                            width="60%" 
                            name="price"
                            type="text" 
                            placeholder="Стоимость" 
                            handleOnChange={getDataFromField}
                        />
                        <InputText 
                            width="40%" 
                            name="quantity"
                            type="text" 
                            placeholder="Количество" 
                            handleOnChange={getDataFromField}
                        />
                    </div>
                    <div style={styles.tools}>
                        <Button text="Добавить" handleOnClick={() => {setDataFromForm(fieldsData)}} />
                    </div>
                </Popup>
            )}
        </div>
    )
}


// class Table extends React.Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             addProductPopUp: {
//                 isActive: true
//             }
//         }

//         this.handleToggler = this.handleToggler.bind(this)
//     }

//     handleToggler() {
//         this.setState(state => ({
//             ...state,
//             addProductPopUp: {
//                 ...state.addProductPopUp,
//                 isActive: !this.state.addProductPopUp.isActive
//             }
//         }))
//     }

//     render() {
//         const { data } = this.props

//         const rows = data.map((row, i) =>
//             <TableRow id={row[0]} title={row[1]} price={row[2]} quantity={row[3]} key={i} />
//         )

//         console.log(this.state)

//         return(
//             <div className="table">
//                 <div className="table__head"></div>
//                 <div className="table__body">
//                     {rows}
//                 </div>
//                 <div className="table__tools">
//                     <Button handleOnClick={() => {this.handleToggler()}} text="Добавить товар" />
//                 </div>
//                 {this.state.addProductPopUp.isActive ? (
//                     <Popup title="Добавление товара" closer={() => {this.handleToggler()}} >
//                         <InputText width="100%" type="text" placeholder="Наименование" />
//                         <InputText width="50%" type="text" placeholder="Стоимость" />
//                         <InputText width="50%" type="text" placeholder="Количество" />
//                     </Popup>
//                 ) : null}
//             </div>
//         )
//     }
// }


const mapDispatchToProps = dispatch => {
    return {
        addProduct: product => {
            dispatch(addProduct(product))
        }
    }
}



export default connect(null, mapDispatchToProps)(Table);