import React from 'react';

import './Popup.scss';


class Popup extends React.Component {
  
  render() {
    const { children, width, title, closer } = this.props

    return (
      <div className="popup">
        <div className="popup__window" style={{width}}>
          <div className="popup__header">
            <div className="popup__title">{title}</div>
            <div className="popup__closer" onClick={() => {closer()}}>Закрыть</div>
          </div>
          <div className="popup__body">
            {children}
          </div>
        </div>
      </div>
    )
  }
}



// const Popup = (props) => {


//   return(
//     <div className="popup">
//       <div className="popup__window">
//         <div className="popup__header">
//           <div className="popup__title"></div>
//           <div className="popup__closer"></div>
//         </div>
//         <div className="popup__body">asd</div>
//       </div>
//     </div>
//   )
// }

export { Popup }