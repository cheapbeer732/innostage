import React from 'react';
import { connect } from 'react-redux';

import './Main.scss';

import { Table } from '../../molecules';
import { Header, Footer } from '../../organisms';

const Main = (props) => {
  const { products } = props

  return(
    <>
      <Header />
      <Table data={products} />
      <Footer />
    </>
  )
}

const mapStateToProps = state => {
  console.log(state)
  return {
      products: state.products.data
  }
}

export default connect(mapStateToProps, null)(Main)