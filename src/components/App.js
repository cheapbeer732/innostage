import React from 'react';


import '../styles/App.scss';

import { Main } from './templates';


class App extends React.PureComponent {
  render() {

    return <div className="app">
      <Main />
    </div>;
  }
}

export default App;
