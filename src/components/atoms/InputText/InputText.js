import React from 'react';

import './InputText.scss';

const InputText = (props) => {
  const { width, name, type, placeholder, handleOnChange } = props

  return(
    <div className="input-text" style={{width: width}}>
      <input
        className="input-text__field" 
        type={type}
        placeholder={placeholder}
        onChange={(e) => {handleOnChange(name, e.target.value)}}
      />
    </div>
  )
}

export { InputText }