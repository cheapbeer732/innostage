import React from 'react';
import { connect } from 'react-redux'

import { removeProduct } from '../../../actions/products';

import './TableRow.scss';
import { Button } from '../Button';

const TableRow = (props) => {
    const { id, title, price, quantity, removeRow } = props

    return(
        <div className="table-row">
            <div className="table-row__cell table-row__cell--title">{title}</div>
            <div className="table-row__cell table-row__cell--price">{price}</div>
            <div className="table-row__cell table-row__cell--quantity">{quantity}</div>
            <div className="table-row__cell table-row__cell--remove">
                <Button handleOnClick={() => {removeRow(id)}} text="x" />
            </div>
        </div>
    )
}


const mapDispatchToProps = dispatch => {
    return {
        removeRow: id => {
            dispatch(removeProduct(id))
        }
    }
}



export default connect(null, mapDispatchToProps)(TableRow);