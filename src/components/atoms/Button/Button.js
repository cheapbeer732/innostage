import React from 'react';

import './Button.scss';

const Button = (props) => {
    const { width, text, handleOnClick } = props

    return(
        <>
            <button className="button" style={{width}} onClick={() => {handleOnClick()}} >{text}</button>
        </>
    )
}

export { Button }