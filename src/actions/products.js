import { ADD_PRODUCT, REMOVE_PRODUCT } from './actionTypes';

export const addProduct = (product) => dispatch => {
  dispatch({
    type: ADD_PRODUCT,
    product: product
  })
}

export const removeProduct = (id) => dispatch => {
  dispatch({
    type: REMOVE_PRODUCT,
    id: id
  })
}