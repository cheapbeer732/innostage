import { ADD_PRODUCT, REMOVE_PRODUCT } from '../../actions/actionTypes';

// const initialState = {
//     data: [
//         [1, "iPhone 5", "400", 5],
//         [2, "XBOX", "300", 7],
//         [3, "PS", "300", 7]
//     ]
// };

const initialState = {
    data: [
        {
            id: 1,
            title: "iPhone 5",
            price: "400",
            quantity: 5
        },
        {
            id: 2,
            title: "XBOX",
            price: "700",
            quantity: 7
        },
        {
            id: 3,
            title: "PS",
            price: "700",
            quantity: 7
        }
    ]
};

export const reducerProducts = (state = initialState, action) => {
    switch(action.type) {
        case ADD_PRODUCT:
            let currentProductId = Number(state.data.length + 1)

            //Если приходит массив
            // action.product.push(currentProductId)
            // action.product.reverse()

            //Если приходит объект
            action.product["id"] = currentProductId

            return {
                ...state,
                data: [...state.data, action.product]
            }

        case REMOVE_PRODUCT:
            return {
                ...state,
                data: state.data.filter(elem => elem.id !== action.id ? true : false)
            }
        default: 
            return state
    }
}