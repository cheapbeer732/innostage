import { combineReducers } from 'redux';
import { reducerProducts } from './products/reducerProducts';

export const rootReducer = combineReducers({
    products: reducerProducts
})